// SPDX-License-Identifier: MIT
pragma solidity ^0.8.23;

import {ERC20} from "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import {Ownable} from "@openzeppelin/contracts/access/Ownable.sol";
import {Lockable} from "./Lockable.sol";

contract LockableTester is Lockable {
    constructor()
        payable
        ERC20("lockableTestCoin", "xxx")
        Ownable(msg.sender)
    {}
}
