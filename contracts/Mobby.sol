// SPDX-License-Identifier: MIT

pragma solidity ^0.8.23;

import {ERC20Permit} from "@openzeppelin/contracts/token/ERC20/extensions/ERC20Permit.sol";
import {ERC20} from "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import {Ownable} from "@openzeppelin/contracts/access/Ownable.sol";

import {Lockable} from "./Lockable.sol";
import {Etherless} from "./Etherless.sol";

string constant tokenName = "Mobby";
string constant tokenSymbol = "MOB";

/**
 * @dev Implementation of the Mobby token. This is a Lockable, Etherless token.
 */
contract Mobby is Lockable, Etherless {
    constructor()
        payable
        ERC20Permit(tokenName)
        ERC20(tokenName, tokenSymbol)
        Ownable(msg.sender)
    {}

    /**
     * @dev performs an etherless transfer from `spender` to `recipient`.
     *
     * Emits an `Approval` and 2 `Transfer` events.
     *
     * Requirements:
     *
     * - `spender` cannot be the zero address.
     * - `deadline` must be a timestamp in the future.
     * - `v`, `r` and `s` must be a valid `secp256k1` signature from `spender`
     * over the EIP712-formatted function arguments.
     * - the signature must use `spender`'s current nonce (see {nonces}).
     * - the value+fee must be covered by the locked balance of the spender. 
     * 
     * It is an error to use unlocked funds for this function and the caller
     * should be checking to verify that a sufficient balance of locked funds
     * is available.
     *
     * For more information on the signature format, see the
     * https://eips.ethereum.org/EIPS/eip-2612#specification[relevant EIP
     * section].
     */
    //solhint-disable-next-line comprehensive-interface
    function transferLockedEtherless(
        address spender,
        address recipient,
        uint256 value,
        uint256 fee,
        uint256 deadline,
        uint8 v,
        bytes32 r,
        bytes32 s
    ) external onlyOwner {
        _unlock(spender, value + fee);
        emit Unlock(spender, 0, value + fee);
        transferEtherless({
            spender: spender,
            recipient: recipient,
            value: value,
            fee: fee,
            deadline: deadline,
            v: v,
            r: r,
            s: s
        });
    }

    /**
     * @dev Set decimals to 6 as opposed to the default of 18
     */
    //solhint-disable-next-line named-return-values
    function decimals() public pure override returns(uint8) {
        return uint8(6);
    }

    /**
     * @dev overrides the update function to check the available balance.
     * While Lockable is an ERC20, the compiler doesn't feel that way about it
     * so we specify which we are using.
     * @param from the address from which to move funds
     * @param to the addres to which to send funds
     * @param value the amount to transfer
     */
    function _update(
        address from,
        address to,
        uint256 value
    ) internal override(Lockable, ERC20) {
        Lockable._update(from, to, value);
    }
}
