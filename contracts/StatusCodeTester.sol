// SPDX-License-Identifier: MIT
pragma solidity >=0.8.0;

import {Status} from "./Status.sol";

contract StatusCodeTester {
    Status.Code public defaultStatusCode;

    constructor(uint8 defaultCode) payable {
        defaultStatusCode = Status.Code(defaultCode);
    }

    //solhint-disable-next-line comprehensive-interface
    function getDefault() public view returns (Status.Code statusCode) {
        return defaultStatusCode;
    }

    // we return majors for each col
    //solhint-disable-next-line comprehensive-interface, code-complexity, function-max-lines
    function getCode(uint8 code) public pure returns (string memory codeStr) {
        if (Status.Code(code) == Status.Code.INFORMATIONAL_OR_METADATA) {
            // 0x0F
            return "INFORMATIONAL_OR_METADATA";
        }
        if (
            Status.Code(code) ==
            Status.Code.PERMISSION_DETAILS_OR_CONTROL_CONDITIONS
        ) {
            // 0x1F
            return "PERMISSION_DETAILS_OR_CONTROL_CONDITIONS";
        }
        if (Status.Code(code) == Status.Code.MATCHING_META_OR_INFO) {
            // 0x2F
            return "MATCHING_META_OR_INFO";
        }
        if (
            Status.Code(code) ==
            Status.Code.NEGOTIATION_RULES_OR_PARTICIPATION_INFO
        ) {
            // 0x3F
            return "NEGOTIATION_RULES_OR_PARTICIPATION_INFO";
        }
        if (Status.Code(code) == Status.Code.AVAILABILITY_RULES_OR_INFO) {
            // 0x4F
            return "AVAILABILITY_RULES_OR_INFO";
        }
        if (Status.Code(code) == Status.Code.TOKEN_OR_FINANCIAL_INFORMATION) {
            // 0x5F
            return "TOKEN_OR_FINANCIAL_INFORMATION";
        }
        if (Status.Code(code) == Status.Code.RESERVED_0x9F) {
            // 0x9F:
            return "RESERVED_0x9F";
        }
        if (Status.Code(code) == Status.Code.APP_SPECIFIC_META_OR_INFO) {
            // 0xAF
            return "APP_SPECIFIC_META_OR_INFO";
        }
        if (Status.Code(code) == Status.Code.RESERVED_0xDF) {
            return "RESERVED_0xDF";
        }
        if (
            Status.Code(code) == Status.Code.CRYPTOGRAPHY_ID_OR_PROOF_METADATA
        ) {
            // 0xEF
            return "CRYPTOGRAPHY_ID_OR_PROOF_METADATA";
        }
        if (Status.Code(code) == Status.Code.OFF_CHAIN_INFO_OR_META) {
            // 0xFF
            return "OFF_CHAIN_INFO_OR_META";
        }
        return "UNKNOWN";
    }
}
