// SPDX-License-Identifier: MIT
// Based on https://github.com/OpenZeppelin/openzeppelin-contracts/
// commit id b4ceb054deba24681aeb50483ccbd4622f37c7e1

pragma solidity ^0.8.23;

import {ERC20} from "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import {ERC20Permit} from "@openzeppelin/contracts/token/ERC20/extensions/ERC20Permit.sol";
import {Etherless} from "./Etherless.sol";

contract EtherlessTester is Etherless {
    constructor()
        payable
        ERC20Permit("etherlessTestCoin")
        ERC20("etherlessTestCoin", "xxx")
    {}

    // solhint-disable-next-line comprehensive-interface
    function mint(address recipient, uint256 amount) public {
        _mint(recipient, amount);
    }
}
