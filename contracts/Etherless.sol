// SPDX-License-Identifier: MIT
// Based on https://github.com/OpenZeppelin/openzeppelin-contracts/
// commit id b4ceb054deba24681aeb50483ccbd4622f37c7e1

pragma solidity ^0.8.23;

import {ERC20Permit} from "@openzeppelin/contracts/token/ERC20/extensions/ERC20Permit.sol";
import {ECDSA} from "@openzeppelin/contracts/utils/cryptography/ECDSA.sol";

/**
 * @dev Implementation of the smart contract functionality required for
 * "Etherless Ethereum Tokens: Simulating Native Tokens in Ethereum":
 * https://eprint.iacr.org/2021/766
 *
 * Adds the {transferEtherless} method, which is used to allow a third party
 * to make transfers. This is very similar to what happens in
 * ERC20Permit (ERC-2612), and we inherit to share some related functions.
 */
abstract contract Etherless is ERC20Permit {
    bytes32 private constant _ETHERLESS_TYPEHASH =
        keccak256(
            "TransferEtherless(address spender,address recipient,address sender,"
            "uint256 value,uint256 fee,uint256 nonce,uint256 deadline)"
        );

    /**
     * @dev Ethereless deadline has expired.
     */
    error EtherlessExpiredSignature(uint256 blockTimestamp, uint256 deadline);

    /**
     * @dev Mismatched signature.
     */
    error EtherlessInvalidSigner(address signer, address owner);

    /**
     * @dev performs an etherless transfer from `spender` to `recipient`.
     *
     * Emits an `Approval` and 2 `Transfer` events.
     *
     * Requirements:
     *
     * - `spender` cannot be the zero address.
     * - `deadline` must be a timestamp of now or in the future.
     * - `v`, `r` and `s` must be a valid `secp256k1` signature from `spender`
     * over the EIP-712 domain-separated encoding of a struct consisting of
     * `spender`, `recipient`, `value`, `fee`, `nonce`, `deadline`.
     * - the signature must use `spender`'s current nonce (see {nonces}). This
     * uses the ERC20-Permit nonce code, which allows only one successful in-flight 
     * transfer of an etherless (or permit) transaction at a time. We recommend
     * short deadlines of an hour or two for this reason. 
     *
     * For more information on the signature format, see the
     * https://eips.ethereum.org/EIPS/eip-2612#specification[relevant EIP
     * section].
     */
    function transferEtherless(
        address spender,
        address recipient,
        uint256 value,
        uint256 fee,
        uint256 deadline,
        uint8 v,
        bytes32 r,
        bytes32 s
    ) public virtual {
        /* solhint-disable not-rely-on-time */
        if (block.timestamp > deadline) {
            revert EtherlessExpiredSignature(block.timestamp, deadline);
        }
        /* solhint-enable not-rely-on-time */

        /* solhint-disable func-named-parameters */
        bytes32 structHash = keccak256(
            abi.encode(
                _ETHERLESS_TYPEHASH,
                spender,
                recipient,
                msg.sender,
                value,
                fee,
                _useNonce(spender),
                deadline
            )
        );
        /* solhint-enable func-named-parameters */
        bytes32 hash = _hashTypedDataV4(structHash);

        address signer = ECDSA.recover(hash, v, r, s);
        if (signer != spender) {
            revert EtherlessInvalidSigner(signer, spender);
        }

        // NOTE: `spender` is the token `owner` in this context
        _approve(spender, msg.sender, value + fee + allowance(spender, msg.sender));
        transferFrom(spender, msg.sender, fee);
        transferFrom(spender, recipient, value);
    }
}
