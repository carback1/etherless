﻿---
title: "Mobby Token Specification"
author: Richard Carback, Julian Cislo, and Mohammed Ishaq
date: January 26, 2024
geometry: margin=1in
output: pdf_document
include-before:
- '`\newpage{}`{=latex}'
---

# Mōbby Token Specification

v0.3

Last Updated: 2/27/2024

By: Richard Carback, Julian Cislo, and Mohammed Ishaq

## Executive Summary
This document is a technical specification and development plan for the smart contract component of the initial release of the Mōbby token. The initial release of the Mōbby token will be on the Ethereum blockchain, so we will be implementing a smart contract that implements the standard token interface (ERC-20) for that smart contract. 

The smart contract is designed to support the following 4 features in the larger Mōbby system:

1. **Token Lockups.** The contract manager is able to control how many tokens are locked for a given user’s wallet address. Some of these locks may be automatically released at predetermined times.
1. **Etherless Transactions.** User’s do not need to hold Ethereum to send and receive the Mōbby token. Instead, they are able to send and receive the Mōbby token by paying the smart contract fees (gas fees) using the Mōbby token itself. This feature leverages the peer-reviewed research paper published by members of the Mōbby project earlier this year. These transactions may be conducted on locked funds with permission by the contract owner.
1. **Black/Gold/Blue Mōbby Wallet types.** Users can hold different wallet types which may or may not have different benefits in the Mōbby network. 
1. **Mōbby Reputation features.** The reputation scores, endorsements, and other Mōbby functions that involve the token are able to be tracked and accounted for efficiently.

The token lockups and etherless transactions have smart contract specific functionalities that enable each property. The wallet types and reputation features are supported by pre-existing functions inside the smart contract standards on Ethereum. 

The wallet types are supported via calls to a standardized mint function and an internal API that will denote the wallet type. The contract management functions will support additional mint operations for rewards and other benefits of different wallet types.

The reputation features are all supported by monitoring the Ethereum chain for events emitted by the contract functions. Certain features are also directly supported by public Mōbby API node calls which will be implemented by the Mōbby node servers. The state tracked by the node servers will enable clean transition to mainnet. 

The Token Lockups are implemented using specific lock and unlock functions. These functions track a balance against the total balance that is locked and unable to be transferred by the users. The lockup function also emit special events which can be used for reputation features.

Etherless transactions are directly and explicitly implemented in the smart contract. We are using the prototype code as a guide and implementing a paired down version that should minimize gas fees on the network. 

\pagebreak

## Introduction

This is a high level functional specification of the Mōbby token as it will be implemented on an Ethereum Virtual Machine (EVM) compatible blockchain. The Mōbby token has 2 features that differentiate it from a vanilla ERC-20:

1. **Token Lockups**: A manager/owner controls how many Mōbby tokens are locked for each address. All tokens are issued in the locked state and converted afterward by the manager/owner. Tokens may not be transferred in a locked state, but the manager/owner can lock unlocked tokens. Tokens may also be unlocked on given time schedules.

1. **Etherless Transactions**: Implements a version of the [“Etherless Ethereum tokens: Simulating native tokens in Ethereum”](https://eprint.iacr.org/2021/766) research paper which allows a delegation server to pay the gas fee and send transactions on behalf of the sender. A special version may allow transfer of locked tokens by the contract owner.

There are other features necessary for Mōbby, but they are implemented by usage of the ERC-20 and other included functionality. Mōbby reputation features are all supported by monitoring the blockchain for the standardized events emitted by the smart contract function calls. The wallet types are implemented using an internal API which maps the wallet address to its type, and the benefits are managed by the contract management tools with the mint functionality.

## Community Standards & Interfaces
These are the interfaces that are widely used by the community. We will support these interfaces in the contract, which will dictate the best starting points in terms of which code we will fork to implement the contract. 

### 1. ERC-20

This is required to be considered a token/coin on-chain. 

<https://github.com/ethereum/ERCs/blob/master/ERCS/erc-20.md> 

```solidity
interface IERC20 {
    function totalSupply() external view returns (uint256);
    function balanceOf(address who) external view returns (uint256);
    function allowance(address owner, address spender)
        external view returns (uint256);
    function transfer(address to, uint256 value)
         external returns (bool);
    function approve(address spender, uint256 value)
        external returns (bool);
    function transferFrom(address from, address to, uint256 value)
         external returns (bool);
    event Transfer(
        address indexed from,
        address indexed to,
        uint256 value
    );
    event Approval(
        address indexed owner,
        address indexed spender,
        uint256 value
    );
}
```

### 2. ERC-5679

Ability to create or destroy Mōbby. 

<https://eips.ethereum.org/EIPS/eip-5679> 

```solidity
interface IERC5679Ext20 {
    function mint(address \_to, uint256 \_amount, 
        bytes calldata \_data) external;
    function burn(address \_from, uint256 \_amount,
        bytes calldata \_data) external;
}
```

### 3. ERC-2612

Enables part of our desired etherless transaction capability by adding an approve call that allows users to modify the allowance mapping using a signed message instead of it having to come directly from the sender. 

<https://eips.ethereum.org/EIPS/eip-2612>

```solidity
interface ERC2612 {
    function permit(address owner, address spender, uint value, 
        uint deadline, uint8 v, bytes32 r, bytes32 s) external
    function nonces(address owner) external view returns (uint)
    function DOMAIN\_SEPARATOR() external view returns (bytes32)
}
```

### 4. EIP-1404

Provides an interface to detect transfer restrictions that is implemented by a lot of the existing tooling out there.

<https://github.com/ethereum/EIPs/issues/1404>

```solidity
contract ERC1404 is ERC20 {
    function detectTransferRestriction (address from, address to,
        uint256 value) public view returns (uint8);
    function messageForTransferRestriction (uint8 restrictionCode)
        public view returns (string);
}
```

Please refer to the links for detailed explanations of the functionality, requirements,and security concerns for each function. 

## Custom Interfaces

These are the custom interfaces we will implement to support functionality specific to the Mōbby Token.

### 1. Token Lockups

Implements the lockup functions used by a manager/owner to control how many Mōbby tokens are locked for each address. Tokens are locked when they are minted to the initial address and by explicitly locking an amount. 

```solidity
function lock(address lockee, uint256 amount);
function unlock(address lockee, uint256 amount);
```

We emit EIP-1132 compatible events for the lock and unlock functions:

<https://github.com/ethereum/EIPs/issues/1132> 

### 2. Etherless Transactions

Implements the functions used by a user via a delegator that will pay the gas fee.

```solidity
function transferEtherless(address spender, address recipient, 
   uint256 value, uint256 fee, uint256 deadline, 
   uint8 v, bytes32 r, bytes32 s);
```

Note that this is the ERC-2612 function parameters with an additional fee parameter, combined with the same return value as the ERC-20 transfer function. You could replace this function with the following call sequence:

```solidity
permit(owner, delegationServer, fee, deadline, v, r, s)
permit(owner, to, value, deadline, v, r, s)
transferFrom(owner, delegationServer, fee)
transferFrom(owner, to, value)
```

The above requires 2 signature verifications instead of 1, and introduces order and timing concerns which are not an issue when wrapped into the transferEtherless function. This requires absolute trust of the delegation server as well. If the delegation server is to send all these transactions, then the permit needs to be made out fully to the delegation server, but then there is the problem of how to incentivize the delegation server not to just pocket the entire allowance, and to actually submit the transferFrom as intended by the user.

## Other Customizations

Our intent is to fork an existing well-known contract code and add the minimum modifications to implement the Mōbby Token. This section provides an overview of the changes we expect for any such base contract in addition to the customizations we described in previous sections. 

### 1. Locked Balance Field

In addition to the \_balances field, we will need an additional \_lockedBalances and  functions to query it: 

```solidity
mapping(address account => uint256) private \_lockedBalances;
function lockedBalanceOf(address who) external view returns (uint256);
function availableBalanceOf(address who) 
    external view returns (uint256);
```

The \_lockedBalances field defined the portion of the balance that is locked. It is enforced by overriding the _update function, which is responsible for avoiding vulnerabilities and issues around mishandling which funds are locked or not.

### 2. ERC-5679 Mint/Burn

These may or may not be available, but the mint will need to be overridden to work with \_lockedBalances only.

### 3. ERC-902 or ERC-1404 Checking functions

These are straightforward to implement and may require custom implementations depending on the base contract.

### 4. ERC-2612 Permit

The functionality is implemented cleanly here: 

<https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/token/ERC20/extensions/ERC20Permit.sol> 

Due to the interaction with transferEtherless, parts of this function will need to be generalized into an internal function with minimal changes. This means we will need to copy/paste this code in and make subsequent changes.

## Required External Function List

In this section we list every external function that is expected to be accessible in the contract. We will include a docgen script in the repository to build out documentation for the main functions of the contract that are not inherited from other code.

```solidity

// ERC20
function allowance(address owner, address spender) view returns (uint256);
function approve(address spender, uint256 value) returns (bool);
function balanceOf(address account) view returns (uint256);
function decimals() view returns (uint8);
function name() view returns (string);
function symbol() view returns (string);
function totalSupply() view returns (uint256);
function transfer(address to, uint256 value) returns (bool);
function transferFrom(address from, address to, uint256 value) returns (bool);

// Locking
function availableBalanceOf(address who) view returns (uint256 availableBalance);
function lock(address lockee, uint256 amount);
function lockedBalanceOf(address who) view returns (uint256 lockedBalance);
function unlock(address lockee, uint256 amount);

// Timed locking
function addTimedUnlock(address who, uint256 when, uint256 amount);
function processTimedUnlocks(address who, uint256 maxToProcess)
   returns (uint256 amountUnlocked);
function timedUnlocksOf(address who) view returns (tuple(uint256 when,
   uint256 amount)[] timedUnlocks);

// Mint & Burn
function burn(address burnFrom, uint256 amount, bytes);
function mint(address recipient, uint256 amount, bytes timeLockBytes);

// ERC20Permit
function nonces(address owner) view returns (uint256);
function permit(address owner, address spender, uint256 value, 
   uint256 deadline, uint8 v, bytes32 r, bytes32 s);

// Etherless
function transferEtherless(address spender, address recipient, 
   uint256 value, uint256 fee, uint256 deadline, 
   uint8 v, bytes32 r, bytes32 s);

// Mobby
function transferLockedEtherless(address spender, address recipient,
   uint256 value, uint256 fee, uint256 deadline,
   uint8 v, bytes32 r, bytes32 s);
```

## Development Notes

Our goal was to minimize review work. We broke the contract up into 3 main parts:

1. `Lockable.sol` - The locking functions and interfaces.
2. `Etherless.sol` - The etherless transfer functionality.
3. `Mobby.sol` - The Mobby specific functionality that did not make sense in one of the other two contracts.

Also included is an enum `Status.sol` library of the standardized ethereum status codes. 

Lockable is a direct implementation of the functionality. For etherless, we started here as our template:

<https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/token/ERC20/extensions/ERC20Permit.sol> 

We used hardhat and ethers to build out tests and solidity-coverage to get line-level test coverage metrics. We also used standard docgen to support the documentation generation. The solhint tool was used for linting, and we also have run slither and mythril on the codebase. 

We did consider implementing EIP-165(<https://eips.ethereum.org/EIPS/eip-165>), but it did not seem useful because we did not have standardized interfaces that needd advertisement in our contracts for our use cases.

Please see the README.md file for instructions on how to compile and test the project.

## Changelog

### v0.1 to 0.2

1. Converted to Markdown format so it can be published in our repository.
1. Replaced development plan section with development notes.
1. Deleted Other considerations section.
1. Updated External functions list with actual list. 
1. Misc. formatting updates for Markdown.
