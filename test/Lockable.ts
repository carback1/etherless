import {
    time,
    loadFixture,
} from "@nomicfoundation/hardhat-toolbox-viem/network-helpers";
import { expect } from "chai";
import hre from "hardhat";
import { hexlify, getBigInt, ZeroAddress, toBigInt, toUtf8Bytes, toBeHex } from "ethers";

const ONE_DAY_IN_SECS = 24 * 60 * 60;
const ONE_YEAR_IN_SECS = 365 * ONE_DAY_IN_SECS;

export function runLockableTests(contractName: string) {
    describe("Lockable", function () {
        async function deployLockableFixture() {
            const [owner, otherAccount] = await hre.ethers.getSigners();

            const lockable = await hre.ethers.deployContract(contractName, [], {});

            return {
                lockable,
                owner,
                otherAccount,
            };
        }

        describe("Deployment", function () {
            it("Sets Owner to deployer", async function () {
                const { lockable, owner } = await loadFixture(deployLockableFixture);

                expect(await lockable.owner()).to.equal(owner.address);
            });

            it("Has empty owner balance", async function () {
                const { lockable, owner } = await loadFixture(deployLockableFixture);

                expect(await lockable.balanceOf(owner.address)).to.equal(0n);
            });

            it("Has empty other account balance", async function () {
                const { lockable, otherAccount } = await loadFixture(deployLockableFixture);

                expect(await lockable.balanceOf(otherAccount.address)).to.equal(0n);
            });

            it("Has empty total supply", async function () {
                const { lockable } = await loadFixture(deployLockableFixture);

                expect(await lockable.totalSupply()).to.equal(0n);
            });

        });

        describe("Locking", function () {
            it("Minted tokens are only mintable by owner and are locked", async function () {
                const { lockable, owner, otherAccount } = await loadFixture(deployLockableFixture);
                const otherAddr = otherAccount.address;
                const ownerAddr = owner.address;

                const otherLockable = new hre.ethers.Contract(
                    await lockable.getAddress(),
                    lockable.interface,
                    otherAccount,
                );

                // non-owner mint
                await expect(otherLockable.mint(otherAddr, 8675309n, hexlify(toUtf8Bytes("Bad Mint")))).to.be.rejectedWith("OwnableUnauthorizedAccount");

                // mint and mint to self
                const startingAmt = 8675309n;

                var tx = await lockable.mint(ownerAddr, startingAmt, hexlify(toUtf8Bytes("self")));
                var receipt = await tx.wait();
                var te = lockable.interface.parseLog(receipt.logs[0]);
                expect(te.name).to.be.equal("Transfer");
                var { from, to, value } = te?.args;
                expect(from).to.be.equal(ZeroAddress);
                expect(to).to.be.equal(ownerAddr);
                expect(value).to.be.equal(startingAmt);
                var le = lockable.interface.parseLog(receipt.logs[1]);
                expect(le.name).to.be.equal("Lock");
                var { lockee, amount } = le?.args;
                expect(lockee).to.be.equal(ownerAddr);
                expect(amount).to.be.equal(startingAmt);


                var tx = await lockable.mint(otherAddr, startingAmt, hexlify(toUtf8Bytes("other")));
                var receipt = await tx.wait();
                var te = lockable.interface.parseLog(receipt.logs[0]);
                expect(te.name).to.be.equal("Transfer");
                var { from, to, value } = te?.args;
                expect(from).to.be.equal(ZeroAddress);
                expect(to).to.be.equal(otherAddr);
                expect(value).to.be.equal(startingAmt);
                var le = lockable.interface.parseLog(receipt.logs[1]);
                expect(le.name).to.be.equal("Lock");
                var { lockee, amount } = le?.args;
                expect(lockee).to.be.equal(otherAddr);
                expect(amount).to.be.equal(startingAmt);

                // lock check
                expect(await lockable.availableBalanceOf(otherAddr)).to.be.equal(0n);
                expect(await lockable.availableBalanceOf(ownerAddr)).to.be.equal(0n);
                expect(await lockable.lockedBalanceOf(otherAddr)).to.be.equal(startingAmt);
                expect(await lockable.lockedBalanceOf(ownerAddr)).to.be.equal(startingAmt);
                expect(await lockable.balanceOf(otherAddr)).to.be.equal(startingAmt);
                expect(await lockable.balanceOf(ownerAddr)).to.be.equal(startingAmt);

                // total supply Check
                const expectedSupply = startingAmt * 2n;
                expect(await lockable.totalSupply()).to.be.equal(expectedSupply);
            });

            it("Locked tokens cannot be transferred", async function () {
                const { lockable } = await loadFixture(deployLockableFixture);
                const [owner, otherAccount, thirdParty] = await hre.ethers.getSigners();
                const otherAddr = otherAccount.address;
                const ownerAddr = owner.address;
                const thirdAddr = thirdParty.address;

                const otherLockable = new hre.ethers.Contract(
                    await lockable.getAddress(),
                    lockable.interface,
                    otherAccount,
                );

                // mint to user
                const startingAmt = 8675309n;
                await lockable.mint(otherAddr, startingAmt, hexlify(toUtf8Bytes("other")));

                // Fail to transfer
                await expect(otherLockable.transfer(thirdAddr, 100n)).to.be.rejectedWith("ERC20InsufficientBalance");

                // Unlock 100 tokens
                var tx = await lockable.unlock(otherAddr, 100n);
                expect(await lockable.availableBalanceOf(otherAddr)).to.be.equal(100n);
                expect(await lockable.lockedBalanceOf(otherAddr)).to.be.equal(startingAmt - 100n);

                // Validate unlock event
                var receipt = await tx.wait();
                var ue = lockable.interface.parseLog(receipt.logs[0]);
                expect(ue.name).to.be.equal("Unlock");
                var { lockee, amount } = ue?.args;
                expect(lockee).to.be.equal(otherAddr);
                expect(amount).to.be.equal(100n);

                // Fail to transfer full bal
                await expect(otherLockable.transfer(thirdAddr, startingAmt)).to.be.rejectedWith("ERC20InsufficientBalance");
                // Fail to transfer full bal + 1
                await expect(otherLockable.transfer(thirdAddr, startingAmt + 1n)).to.be.rejectedWith("ERC20InsufficientBalance");
                // Fail to transfer 101
                await expect(otherLockable.transfer(thirdAddr, 101n)).to.be.rejectedWith("ERC20InsufficientBalance");
                const neg1 = toBigInt("0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF");
                // Fail to transfer neg1
                await expect(otherLockable.transfer(thirdAddr, neg1)).to.be.rejectedWith("ERC20InsufficientBalance");
                // Succeed to transfer 100
                await otherLockable.transfer(thirdAddr, 100n);
                // Fail to transfer 1
                await expect(otherLockable.transfer(thirdAddr, 1n)).to.be.rejectedWith("ERC20InsufficientBalance");
                // Fail to transfer neg1
                await expect(otherLockable.transfer(thirdAddr, neg1)).to.be.rejectedWith("ERC20InsufficientBalance");

                expect(await lockable.availableBalanceOf(otherAddr)).to.be.equal(0n);
                expect(await lockable.lockedBalanceOf(otherAddr)).to.be.equal(startingAmt - 100n);
                expect(await lockable.availableBalanceOf(thirdAddr)).to.be.equal(100n);
                expect(await lockable.lockedBalanceOf(thirdAddr)).to.be.equal(0n);

                // total supply Check
                const expectedSupply = startingAmt;
                expect(await lockable.totalSupply()).to.be.equal(expectedSupply);
            })

            it("Unlock and lock lock balances and do not affect supply", async function () {
                const { lockable, owner, otherAccount } = await loadFixture(deployLockableFixture);
                const otherAddr = otherAccount.address;
                const ownerAddr = owner.address;

                const otherLockable = new hre.ethers.Contract(
                    await lockable.getAddress(),
                    lockable.interface,
                    otherAccount,
                );

                // mint to user
                const startingAmt = 8675309n;
                await lockable.mint(otherAddr, startingAmt, hexlify(toUtf8Bytes("other")));

                // Unlock 100 tokens
                await lockable.unlock(otherAddr, 100n);
                expect(await lockable.availableBalanceOf(otherAddr)).to.be.equal(100n);
                expect(await lockable.lockedBalanceOf(otherAddr)).to.be.equal(startingAmt - 100n);
                expect(await lockable.totalSupply()).to.be.equal(startingAmt);

                // Fail to lock 101 tokens
                await expect(lockable.lock(otherAddr, 101n)).to.be.rejectedWith("LockableInsufficientFunds");

                // Lock 99 Tokens
                await lockable.lock(otherAddr, 99n);
                expect(await lockable.availableBalanceOf(otherAddr)).to.be.equal(1n);
                expect(await lockable.lockedBalanceOf(otherAddr)).to.be.equal(startingAmt - 1n);
                expect(await lockable.totalSupply()).to.be.equal(startingAmt);

                // Lock 1 tokens
                await lockable.lock(otherAddr, 1n);
                expect(await lockable.availableBalanceOf(otherAddr)).to.be.equal(0n);
                expect(await lockable.lockedBalanceOf(otherAddr)).to.be.equal(startingAmt);
                expect(await lockable.totalSupply()).to.be.equal(startingAmt);
            });
            it("Burning decreases total supply, even when locked", async function () {
                const { lockable, owner, otherAccount } = await loadFixture(deployLockableFixture);
                const otherAddr = otherAccount.address;
                const ownerAddr = owner.address;

                const otherLockable = new hre.ethers.Contract(
                    await lockable.getAddress(),
                    lockable.interface,
                    otherAccount,
                );

                // mint to user
                const startingAmt = 8675309n;
                const startingSupply = startingAmt * 2n;
                await lockable.mint(ownerAddr, startingAmt, hexlify(toUtf8Bytes("other")));
                await lockable.mint(otherAddr, startingAmt, hexlify(toUtf8Bytes("other")));

                // fail to burn as owner
                await expect(lockable.burn(otherAddr, 1n, hexlify(toUtf8Bytes("bad burn")))).to.be.rejectedWith("ERC20InvalidSender");

                // Unlock 100 tokens
                await lockable.unlock(otherAddr, 100n);
                expect(await lockable.availableBalanceOf(otherAddr)).to.be.equal(100n);
                expect(await lockable.lockedBalanceOf(otherAddr)).to.be.equal(startingAmt - 100n);
                expect(await lockable.totalSupply()).to.be.equal(startingSupply);

                // Burn a token
                await otherLockable.burn(otherAddr, 1n, hexlify(toUtf8Bytes("good burn")));
                var curSupply = startingSupply - 1n;
                expect(await lockable.availableBalanceOf(otherAddr)).to.be.equal(99n);
                expect(await lockable.lockedBalanceOf(otherAddr)).to.be.equal(startingAmt - 100n);
                expect(await lockable.totalSupply()).to.be.equal(curSupply);

                // Burn tokens, even a locked one
                await otherLockable.burn(otherAddr, 100n, hexlify(toUtf8Bytes("good burn 2")));
                curSupply -= 100n;
                expect(await lockable.availableBalanceOf(otherAddr)).to.be.equal(0n);
                expect(await lockable.lockedBalanceOf(otherAddr)).to.be.equal(startingAmt - 101n);
                expect(await lockable.totalSupply()).to.be.equal(curSupply);

                // Burn the rest of the locked tokens
                await otherLockable.burn(otherAddr, startingAmt - 101n, hexlify(toUtf8Bytes("good burn 3")));
                curSupply -= startingAmt - 101n;
                expect(await lockable.availableBalanceOf(otherAddr)).to.be.equal(0n);
                expect(await lockable.lockedBalanceOf(otherAddr)).to.be.equal(0n);
                expect(await lockable.totalSupply()).to.be.equal(curSupply);

                // Fail to burn more than that
                await expect(otherLockable.burn(otherAddr, 1n, hexlify(toUtf8Bytes("bad burn 2")))).to.be.rejectedWith("ERC20InsufficientBalance");

            })
            it("Invalid locks/unlocks produce errors", async function () {
                const { lockable, owner, otherAccount } = await loadFixture(deployLockableFixture);
                const otherAddr = otherAccount.address;
                const ownerAddr = owner.address;

                await expect(lockable.lock(ZeroAddress, 100n)).to.be.rejectedWith("LockableInvalidAddress");

                await expect(lockable.unlock(ZeroAddress, 100n)).to.be.rejectedWith("LockableInvalidAddress");

                await expect(lockable.unlock(otherAddr, 100n)).to.be.rejectedWith("LockableInsufficientFunds");


            });
        });
        describe("Timed Locking", async function () {
            it("Timed Unlocks when minting emit proper events and data structures", async function () {
                const { lockable, otherAccount } = await loadFixture(deployLockableFixture);
                const startingAmt = 1000n;
                const oneYear = toBeHex(BigInt((await time.latest()) + ONE_YEAR_IN_SECS), 32);
                const tomorrow = toBeHex(BigInt((await time.latest()) + ONE_DAY_IN_SECS), 32);
                const nextWeek = toBeHex(BigInt((await time.latest()) + 7 * ONE_DAY_IN_SECS), 32);

                const otherAddr = otherAccount.address;

                // Mint for 1 week out
                var curAmt = startingAmt;
                var tx = await lockable.mint(otherAddr, startingAmt, nextWeek);
                expect(await lockable.availableBalanceOf(otherAddr)).to.be.equal(0n);
                expect(await lockable.lockedBalanceOf(otherAddr)).to.be.equal(startingAmt);

                // Validate lock event
                var receipt = await tx.wait();
                var le = lockable.interface.parseLog(receipt.logs[1]);
                expect(le.name).to.be.equal("Lock");
                var { lockee, reason, amount, validUntil } = le?.args;
                expect(lockee).to.be.equal(otherAddr);
                expect(amount).to.be.equal(curAmt);
                expect(reason).to.be.equal(1);
                expect(validUntil).to.be.equal(nextWeek);

                // one year out
                curAmt += startingAmt;
                var tx = await lockable.mint(otherAddr, startingAmt, oneYear);
                expect(await lockable.availableBalanceOf(otherAddr)).to.be.equal(0n);
                expect(await lockable.lockedBalanceOf(otherAddr)).to.be.equal(curAmt);

                // Validate lock event
                var receipt = await tx.wait();
                var le = lockable.interface.parseLog(receipt.logs[1]);
                expect(le.name).to.be.equal("Lock");
                var { lockee, reason, amount, validUntil } = le?.args;
                expect(lockee).to.be.equal(otherAddr);
                expect(amount).to.be.equal(startingAmt);
                expect(reason).to.be.equal(1);
                expect(validUntil).to.be.equal(oneYear);

                // tomorrow
                curAmt += startingAmt;
                var tx = await lockable.mint(otherAddr, startingAmt, tomorrow);
                expect(await lockable.availableBalanceOf(otherAddr)).to.be.equal(0n);
                expect(await lockable.lockedBalanceOf(otherAddr)).to.be.equal(curAmt);

                // Validate lock event
                var receipt = await tx.wait();
                var le = lockable.interface.parseLog(receipt.logs[1]);
                expect(le.name).to.be.equal("Lock");
                var { lockee, reason, amount, validUntil } = le?.args;
                expect(lockee).to.be.equal(otherAddr);
                expect(amount).to.be.equal(startingAmt);
                expect(reason).to.be.equal(1);
                expect(validUntil).to.be.equal(tomorrow);


                // Verify they are correct order in the data structure
                var tx = await lockable.timedUnlocksOf(otherAddr);
                var expectedTimes = [ // Oldest first
                    oneYear,
                    nextWeek,
                    tomorrow,
                ]
                for (var i = 0; i < expectedTimes.length; i++) {
                    expect(getBigInt(tx[i][0])).to.be.equal(expectedTimes[i]);
                    expect(tx[i][1]).to.be.equal(startingAmt);
                }
            });
            it("Invalid Timed Unlocks cause errors", async function () {
                const { lockable, owner, otherAccount } = await loadFixture(deployLockableFixture);
                const startingAmt = 1000n;
                const yesterday = toBeHex(BigInt((await time.latest()) - ONE_DAY_IN_SECS), 32);
                const earlier = toBeHex(BigInt((await time.latest()) - 1), 32);
                const now = toBeHex(BigInt((await time.latest())), 32);
                const otherAddr = otherAccount.address;

                await lockable.mint(otherAddr, 3n * startingAmt, hexlify(toUtf8Bytes("")));

                // 1 day ago
                await expect(lockable.addTimedUnlock(otherAddr, startingAmt, yesterday)).to.be.rejectedWith("LockableTimeInThePast");
                // 1 second ago
                await expect(lockable.addTimedUnlock(otherAddr, startingAmt, earlier)).to.be.rejectedWith("LockableTimeInThePast");
                // now
                await expect(lockable.addTimedUnlock(otherAddr, startingAmt, now)).to.be.rejectedWith("LockableTimeInThePast");
            });
            it("Timed Unlocks unlock tokens appropriately", async function () {
                const { lockable, owner, otherAccount } = await loadFixture(deployLockableFixture);
                const startingAmt = 1000n;
                const oneYear = toBeHex(BigInt((await time.latest()) + ONE_YEAR_IN_SECS), 32);
                const tomorrow = toBeHex(BigInt((await time.latest()) + ONE_DAY_IN_SECS), 32);
                const nextWeek = toBeHex(BigInt((await time.latest()) + 7 * ONE_DAY_IN_SECS), 32);
                const twoWeek = toBeHex(BigInt((await time.latest()) + 14 * ONE_DAY_IN_SECS), 32);

                const otherLockable = new hre.ethers.Contract(
                    await lockable.getAddress(),
                    lockable.interface,
                    otherAccount,
                );

                const otherAddr = otherAccount.address;

                // Mint for 1 week out
                var curAmt = startingAmt;
                await lockable.mint(otherAddr, startingAmt, nextWeek);
                // one year out
                curAmt += startingAmt;
                await lockable.mint(otherAddr, startingAmt, oneYear);
                // tomorrow
                curAmt += startingAmt;
                await lockable.mint(otherAddr, startingAmt, hexlify(toUtf8Bytes("")));
                await lockable.addTimedUnlock(otherAddr, tomorrow, startingAmt);

                expect(await lockable.availableBalanceOf(otherAddr)).to.be.equal(0n);
                expect(await lockable.lockedBalanceOf(otherAddr)).to.be.equal(curAmt);
                expect(await lockable.availableBalanceOf(owner)).to.be.equal(0n);
                expect(await lockable.lockedBalanceOf(owner)).to.be.equal(0n);

                // Can't spend now
                await expect(otherLockable.transfer(owner, 100n)).to.be.rejectedWith("ERC20InsufficientBalance");

                // verify tomorrow's unlocks on a transfer call
                await time.increaseTo(tomorrow);
                var tx = await otherLockable.transfer(owner, startingAmt);
                var receipt = await tx.wait();
                var ue = otherLockable.interface.parseLog(receipt.logs[0]);
                expect(ue.name).to.be.equal("Unlock");
                var { lockee, reason, amount } = ue?.args;
                expect(lockee).to.be.equal(otherAddr);
                expect(amount).to.be.equal(startingAmt);
                expect(reason).to.be.equal(1n);


                await expect(otherLockable.transfer(owner, 100n)).to.be.rejectedWith("ERC20InsufficientBalance");
                expect(await lockable.availableBalanceOf(otherAddr)).to.be.equal(0n);
                expect(await lockable.lockedBalanceOf(otherAddr)).to.be.equal(curAmt - startingAmt);
                expect(await lockable.availableBalanceOf(owner)).to.be.equal(startingAmt);
                expect(await lockable.lockedBalanceOf(owner)).to.be.equal(0n);

                // verify 1 week unlocks at the 2 week call
                await time.increaseTo(twoWeek);
                otherLockable.transfer(owner, startingAmt);
                await expect(otherLockable.transfer(owner, 100n)).to.be.rejectedWith("ERC20InsufficientBalance");
                expect(await lockable.availableBalanceOf(otherAddr)).to.be.equal(0n);
                expect(await lockable.lockedBalanceOf(otherAddr)).to.be.equal(curAmt - 2n * startingAmt);
                expect(await lockable.availableBalanceOf(owner)).to.be.equal(2n * startingAmt);
                expect(await lockable.lockedBalanceOf(owner)).to.be.equal(0n);


                // Unlock but do not transfer after 1 year
                await time.increaseTo(oneYear);
                var tx = await otherLockable.processTimedUnlocks(otherAddr, 10n);
                var receipt = await tx.wait();
                var ue = otherLockable.interface.parseLog(receipt.logs[0]);
                expect(ue.name).to.be.equal("Unlock");
                var { lockee, reason, amount } = ue?.args;
                expect(lockee).to.be.equal(otherAddr);
                expect(amount).to.be.equal(startingAmt);
                expect(reason).to.be.equal(1n);

                expect(await lockable.availableBalanceOf(otherAddr)).to.be.equal(curAmt - 2n * startingAmt);
                expect(await lockable.lockedBalanceOf(otherAddr)).to.be.equal(0n);
                expect(await lockable.availableBalanceOf(owner)).to.be.equal(2n * startingAmt);
                expect(await lockable.lockedBalanceOf(owner)).to.be.equal(0n);


            });
            it("Will not set timed lock when no locked balance", async function () {
                const { lockable, otherAccount } = await loadFixture(deployLockableFixture);
                const startingAmt = 1000n;
                const tomorrow = toBeHex(BigInt((await time.latest()) + ONE_DAY_IN_SECS), 32);

                const otherAddr = otherAccount.address;

                await lockable.mint(otherAddr, startingAmt, hexlify(toUtf8Bytes("")));
                await lockable.unlock(otherAddr, startingAmt);
                await expect(lockable.addTimedUnlock(otherAddr, tomorrow, startingAmt)).to.be.rejectedWith("LockableInsufficientFunds");
                expect(await lockable.availableBalanceOf(otherAddr)).to.be.equal(startingAmt);
                expect(await lockable.lockedBalanceOf(otherAddr)).to.be.equal(0n);

                // Verify they are correct order in the data structure
                var tx = await lockable.timedUnlocksOf(otherAddr);
                expect(tx.length).to.be.equal(0);
            });
            it("Burning/Unlocking doesn't break timed unlocking", async function () {
                const { lockable, owner, otherAccount } = await loadFixture(deployLockableFixture);
                const startingAmt = 1000n;
                const oneYear = toBeHex(BigInt((await time.latest()) + ONE_YEAR_IN_SECS), 32);
                const twoYears = toBeHex(BigInt((await time.latest()) + 2 * ONE_YEAR_IN_SECS), 32);
                const threeYears = toBeHex(BigInt((await time.latest()) + 3 * ONE_YEAR_IN_SECS), 32);
                const tomorrow = toBeHex(BigInt((await time.latest()) + ONE_DAY_IN_SECS), 32);
                const otherAddr = otherAccount.address;
                const otherLockable = new hre.ethers.Contract(
                    await lockable.getAddress(),
                    lockable.interface,
                    otherAccount,
                );
                await lockable.mint(otherAddr, startingAmt, oneYear);
                await lockable.mint(otherAddr, startingAmt, tomorrow);
                // The tomorrow mint should work fully, the other only 50%. 
                await lockable.unlock(otherAddr, startingAmt / 2n);
                await time.increaseTo(oneYear);

                var tx = await otherLockable.processTimedUnlocks(otherAddr, 10);
                var receipt = await tx.wait();
                var ue = otherLockable.interface.parseLog(receipt.logs[0]);
                expect(ue.name).to.be.equal("Unlock");
                var { lockee, reason, amount } = ue?.args;
                expect(lockee).to.be.equal(otherAddr);
                expect(amount).to.be.equal(startingAmt + startingAmt / 2n);
                expect(reason).to.be.equal(1n);

                expect(await lockable.availableBalanceOf(otherAddr)).to.be.equal(2n * startingAmt);
                expect(await lockable.lockedBalanceOf(otherAddr)).to.be.equal(0n);

                // Now try with burning
                await lockable.lock(otherAddr, 2n * startingAmt);
                await lockable.addTimedUnlock(otherAddr, twoYears, startingAmt);
                await lockable.addTimedUnlock(otherAddr, threeYears, startingAmt);
                await otherLockable.burn(otherAddr, startingAmt + startingAmt / 2n, hexlify(toUtf8Bytes("")));
                await time.increaseTo(threeYears);

                var tx = await otherLockable.processTimedUnlocks(otherAddr, 10);
                var receipt = await tx.wait();
                var ue = otherLockable.interface.parseLog(receipt.logs[0]);
                expect(ue.name).to.be.equal("Unlock");
                var { lockee, reason, amount } = ue?.args;
                expect(lockee).to.be.equal(otherAddr);
                expect(amount).to.be.equal(startingAmt / 2n);
                expect(reason).to.be.equal(1n);

                expect(await lockable.availableBalanceOf(otherAddr)).to.be.equal(startingAmt / 2n);
                expect(await lockable.lockedBalanceOf(otherAddr)).to.be.equal(0n);





            });
        });
    });
}

runLockableTests("LockableTester");