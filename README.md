# Mobby Token

The Mobby token is an etherless, lockable token based on the OpenZeppelin Library.

To install and test this contract locally, you need nodejs installed. Then you can
run the following commands:

```shell
npm install
npx hardhat compile # compile solidity contracts
npx hardhat test # run unit tests
REPORT_GAS=true npx hardhat test # run unit tests with gas coverage
npx hardhat coverage # run code coverage tool
npx hardhat check # run linter
```

## Deploying

Configure your network, e.g., sepolia, inside of hardhat.config.ts, then run the following:

```
npx hardhat run --network sepolia scripts/deploy.ts 
```

After this deploys, in a few minutes you will be able to run the verifier with the provided contract address: 

```
npx hardhat verify --network sepolia 0x903Aa2aDd5d7B224874b6D8Ed9Be657Ca70A55C5 
```

Note that Sourcify might fail, you can run the verification for that manually at https://sourcify.dev/

## Changing Ownership

To use the change ownership task, you can run: 

```
npx hardhat --network sepolia changeOwner Mobby "0x903Aa2aDd5d7B224874b6D8Ed9Be657Ca70A55C5" "0xcD50D0D4f978281098BF5Bf6aD74b6985B424bBc"
```

The positional arguments are: 
1. The name of the contract (`Mobby`, see `scripts/deploy.ts`)
2. The contract Address from the deployment step
3. The new owner address

## Initial Mint

```
npx hardhat --network sepolia mint Mobby 0x4AbDCf1C5C4a88C5832b9Ad3350dfC1923d2079C Mōbby\ Ethereum\ Token\ -\ Data\ Collection\ \(Responses\)\ -\ Form\ Responses\ 1.csv
```

## Unlocking Tokens

```
npx hardhat --network sepolia unlock Mobby 0x4AbDCf1C5C4a88C5832b9Ad3350dfC1923d2079C 0xCFca6375C440BD79325ecD5e37aC4EaB24CB3362 5000
```