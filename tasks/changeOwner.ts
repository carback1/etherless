import { task } from "hardhat/config";

task("changeOwner", "Changes the owner on the provided contract")
    .addPositionalParam("contractName", "The deployed contract name")
    .addPositionalParam("contractAddress", "The contract address")
    .addPositionalParam("walletAddress", "The new owner's wallet address")
    .setAction(async (taskArgs, hre) => {
        console.log(taskArgs, hre)
        const contractAddress = taskArgs.contractAddress;
        const contract = await hre.ethers.getContractAt(taskArgs.contractName,
            contractAddress);

        const [ownerWallet] = await hre.ethers.getSigners();

        const listedOwner = await contract.owner();
        if (listedOwner != ownerWallet.address) {
            console.log("Listed Owner != Provided Owner", listedOwner, ownerWallet.address);
            console.log("In hardhat.config.ts, set the private key for this address: ", listedOwner);
            return -1;
        }

        const newOwnerAddress = taskArgs.walletAddress;
        console.log(`Changing owner on ${contractAddress} from ${listedOwner} to ${newOwnerAddress}`);

        const res = await contract.transferOwnership(newOwnerAddress);
        console.log(`transferOwnership(${newOwnerAddress}):`, res)
});
